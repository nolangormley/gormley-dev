from smtplib import SMTP
import datetime
from os import listdir
from os.path import isfile, join
from jinja2 import Template
import smtplib
import ssl
import cherrypy
import json

class Blog(object):
    @cherrypy.expose
    def __init__(self):
        self.posts = self.getPosts()

    def _cp_dispatch(self, vpath):
        name = None
        if len(vpath) == 1:
            name = vpath.pop()

        post = list(filter(lambda posts: posts['post-link'] == name, self.posts)).pop()
        
        with open(post['post-file']) as post:
            cherrypy.request.params['current_post'] = post.read()

        return self

    @cherrypy.expose
    def index(self, current_post=''):
        with open('./templates/blog.html') as file_:
            template = Template(file_.read())
        with open('./templates/navbar.html') as file_:
            navbar_raw = Template(file_.read())
        if current_post == '':
            with open('./templates/welcome.html') as file_:
                current_post = file_.read()

        return template.render(navbar=navbar_raw.render(), posts=self.posts, current_post=current_post)

    def getPosts(self):
        with open('./posts.json') as data:
            posts = json.load(data)
        posts.sort(key = lambda x:x['post-date'], reverse=True)
        return posts

class website(object):

    blog = Blog()

    @cherrypy.expose
    def index(self):
        with open('templates/index.html') as file_:
            template = Template(file_.read())
        with open('templates/navbar.html') as file_:
            navbar_raw = Template(file_.read())

        return template.render(navbar=navbar_raw.render())

    @cherrypy.expose
    def resume(self):
        with open('templates/resume.html') as file_:
            template = Template(file_.read())
        with open('templates/navbar.html') as file_:
            navbar_raw = Template(file_.read())

        return template.render(navbar=navbar_raw.render())

    @cherrypy.expose
    def contact(self, sent=False):
        with open('templates/contact.html') as file_:
            template = Template(file_.read())
        with open('templates/navbar.html') as file_:
            navbar_raw = Template(file_.read())

        return template.render(navbar=navbar_raw.render(), sent=sent)

    @cherrypy.expose
    def shatnerSlides(self):
        with open('templates/shatnerslides.html') as file_:
            template = Template(file_.read())

        return template.render()

    @cherrypy.expose
    #@cherrypy.tools.json_in()
    def email(self, name=None, email=None, subject=None):
        # import pdb; pdb.set_trace()
        # from_addr = "gormley.dev@gmail.com"
        # to_addr = "nolangormley@gmail.com"
        # subj = "Email from gormley.dev Contact Page"
        # date = datetime.datetime.now().strftime( "%d/%m/%Y %H:%M" )

        # message_text = ("Hello\nThis is a mail from your server\n\n Please contact %s at %s\n They say: %s"
        #                     % ( name, email, subject ))

        # msg = ("From: %s\nTo: %s\nSubject: %s\nDate: %s\n\n%s" 
        #         % ( from_addr, to_addr, subj, date, message_text ))

        # server = smtplib.SMTP('smtp.gmail.com', 25)
        # server.connect("smtp.gmail.com",587)
        # server.ehlo()
        # server.starttls()
        # server.ehlo()
        # server.login(from_addr, "Superfuckingman1997!")
        # server.sendmail(from_addr, to_addr, msg)
        # server.quit()

        # return website.contact(self, sent=True)
        raise cherrypy.HTTPRedirect("/contact?sent=True")

        

cherrypy.quickstart(website(), '/', "./conf/app.conf")
